// import express (after npm install express)
const express = require('express');
const { exec } = require('child_process');
const bodyParser = require('body-parser')
// create new express app and save it as "app"
const app = express();


//app.use(cors());
// server configuration
const PORT = 4000;

// create a route for the app
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/deploy-api', function (req, res) {
  console.log("req.body.ref", req.body.ref);
  let script_file = "dev-api.sh"
  if (req.body.ref === "refs/heads/master")   script_file = "master-api.sh"
  if (req.body.ref === "refs/heads/develop")  script_file = "dev-api.sh"


  if (req.headers['x-gitlab-token'] === '70469e0d-200f-4834-8f35-4e94532eb7ea') {
    // exec('ssh root@51.77.194.158 "cd /var/www/AIO_GIT && pm2 deploy production setup"',
    try {
      console.log('=============+++++++++++++++> api deploy', script_file);
      const testscript = exec('sh '+script_file);

      testscript.stdout.on('data', function (data) {
        console.log(data);
        // sendBackInfo();
      });

      testscript.stderr.on('data', function (data) {
        console.log(data);
        // triggerErrorStuff();
      });
      console.log('Success');
      return res.status(200).json({ msg: 'Deploy post token' });
    } catch (error) {
      console.log('Error');
      return res.status(500).json({ msg: 'Error serveur' });
    }
  } 
  else{
    console.log('UNAUTHORIZED');
    return res.status(401).json({ msg: 'ERROR' });  
  }
})
// create a route for the app
app.post('/deploy-web', function (req, res) {
  if (req.body.ref === "refs/heads/master") {
    if (req.headers['x-gitlab-token'] == '70469e0d-200f-4834-8f35-4e94532eb7ea') {
      try {
      console.log('preprod web deploy');
      const testscript = exec('sh master-web.sh');

      testscript.stdout.on('data', function (data) {
        console.log(data);
        // sendBackInfo();
      });

      testscript.stderr.on('data', function (data) {
        console.log(data);
        // triggerErrorStuff();
      });
      console.log('Success');
      return res.status(200).json({ msg: 'Deploy post token' });
    } catch (error) {
      console.log('Error');
      return res.status(500).json({ msg: 'Error serveur' });
    }
    } else {
      console.log('UNAUTHORIZED');
      return res.status(401).json({ msg: 'ERROR' });

    }
  }
  if (req.body.ref === "refs/heads/develop") {

    if (req.headers['x-gitlab-token'] == '70469e0d-200f-4834-8f35-4e94532eb7ea') {
      try {
        console.log('develop web deploy');
        const testscript = exec('sh dev-web.sh');

        testscript.stdout.on('data', function (data) {
          console.log(data);
          // sendBackInfo();
        });

        testscript.stderr.on('data', function (data) {
          console.log(data);
          // triggerErrorStuff();
        });
        console.log('Success');
        return res.status(200).json({ msg: 'Deploy post token' });
      } catch (error) {
        console.log('Error');
        return res.status(500).json({ msg: 'Error serveur' });
      }

    } else {
      console.log('UNAUTHORIZED');
      return res.status(401).json({ msg: 'ERROR' });

    }
  }
})


app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});
