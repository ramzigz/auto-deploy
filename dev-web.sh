cd  /var/www/green-ride/web_dev
git checkout develop
git pull origin develop
npm install
pm2 stop grideweb
pm2 delete grideweb
pm2 flush
pm2 start 'npm start' --name grideweb
